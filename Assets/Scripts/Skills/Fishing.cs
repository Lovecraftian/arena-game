﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fishing : MonoBehaviour
{
    float fishRates = 0f;
    // Start is called before the first frame update
    float fishTimer = 1f;
    float fishTime = 0f;
    void Start()
    {
       fishRates = transform.root.gameObject.GetComponent<Player>().fishRates;
    }

    // Update is called once per frame
    void Update()
    {
        
        fishTime += Time.deltaTime;
        if(fishTime >= 1f)
        {
            fishTime = 0f;
            if (transform.root.gameObject.GetComponent<Player>().isFishing)
            {
                if (Random.value < fishRates)
                {
                   // Debug.Log("Fish catched!");
                }
                
            }
        }
        
    }
}
