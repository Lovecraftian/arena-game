﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;


public class Skelekton : Enemy
{
    protected int attackRange = 5;

    // Start is called before the first frame update
    void Start()
    {
        _stateMachine = new StateMachine(); 

        m_agent = this.GetComponent<NavMeshAgent>();
        _target = PlayerManager.m_instance.m_player.transform;
        anim = this.GetComponent<Animator>();

        _stateMachine.ChangeState(new IdleState(anim, this.gameObject, m_Lookradius, _target,  _stateMachine));
    }

    // Update is called once per frame
    void Update()
    {
        _stateMachine.Update();

        // Brakycs sin chase code (cheks if player is withinn radius)
        float distance = Vector3.Distance(_target.position, transform.position);

        if (distance <= m_Lookradius)
        {
            // m_agent.SetDestination(_target.position);
            // anim.SetBool("isWalking", true); // swap this so statemachines does it
 
            if (distance <= m_agent.stoppingDistance) // dont think this code is ever executed tbh
            {
                Debug.Log("is within agent stopping distance");
                // Attack the target
                // Face the target
                FaceTarget();
            }
            if (anim)
            {
                if (distance < attackRange)
                {
                    anim.SetBool("isWalking", false);
                    anim.SetBool("isAttacking", true); // swap this so statemachines does it
                }
                else
                {

                    anim.SetBool("isAttacking", false); // swap this so statemachines does it
                }
            }
        }
    }

}



