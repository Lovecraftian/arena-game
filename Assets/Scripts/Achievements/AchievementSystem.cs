﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementSystem : Observer
{
    [SerializeField]
    private AudioSource sound;
    public GameObject achievementTitle;
    public GameObject achievementDesc;
    public GameObject achievement;

    private void Start()
    {
        PlayerPrefs.DeleteAll();

        foreach(PointOfInterest poi in FindObjectsOfType<PointOfInterest>())
        {
            poi.RegisterObserver(this);
        }
        
    }

    public override void OnNotify(object value, NotificationType type)
    {
        if(type == NotificationType.AchievementUnlocked)
        {
            string achievementName = ""+value;

            if(PlayerPrefs.GetInt(achievementName) == 0)
            {
                PlayerPrefs.SetInt(achievementName, 1);
               
                ShowAchievement(achievementName);
            }
        }
    }

    private void ShowAchievement(string achievementName)
    {
        

        achievement.SetActive(true);
        StartCoroutine("TurnOffAchievement");
        string achievementDescription = "";
        switch (achievementName)
        {
            case "Give me gems!": achievementDescription = "You have found an ore! Let's mine it! (press F). Ores can be used to enhance your weapon"; break;
            case "Chop chop!": achievementDescription = "This tree can be chopped down! (press F)"; break;
            case "Bash the skelektons!": achievementDescription = "The arena is a test of strenght and willpower. Show them what you've got! (use mouse button to attack and shield)"; break;
            case "Fishing break!": achievementDescription = "You must be tired. Maybe some fishing will help? (press F close to water and wait). Eating fish på pressing H heals you a little."; break;
        }
        Debug.Log(achievementName);
        //sound.Play();
        achievementTitle.GetComponent<Text>().text = achievementName;
        achievementDesc.GetComponent<Text>().text = achievementDescription;
    }

    IEnumerator TurnOffAchievement()
    {
        yield return new WaitForSeconds(5);
        achievement.SetActive(false);
    }
}



public enum NotificationType
{
    AchievementUnlocked
}