﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Dialouge : MonoBehaviour
{
    [SerializeField]
    public GameObject dialogueBox;
    public bool PlayerHasEntered = false;

    public GameObject enterTextPrefab;
    private GameObject textObject;
    Player player;
    Button enhanceButton;
    [SerializeField] public string type;

    string info = "Press [E] to talk";

    [SerializeField]
    public Vector3 offset;

    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            player = other.gameObject.GetComponent<Player>();
            if (type == "blacksmith")
            {
                enhanceButton = GameObject.Find("UI/Canvas/DialoigueUI/DialogueBoxBlackSmith/enhancebutton").gameObject.GetComponent<Button>();
                enhanceButton.onClick.AddListener(BuyEnhance);
            }
            else
            {
                Debug.Log("is merchant");
                enhanceButton = GameObject.Find("UI/Canvas/DialoigueUI/DialogueBoxMerchant/Donate").gameObject.GetComponent<Button>();
                enhanceButton.onClick.AddListener(GiveLog);
            }
            
            ShowEnterText();
        }
    }
    public void BuyEnhance()
    {
        
        if (player)
        player.BuyEnhance();
    }
    public void GiveLog()
    {
        Debug.Log("clicked log");
        if (player)
            player.GiveLog();
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (Input.GetKeyDown(KeyCode.E) )
            {
                if(PlayerHasEntered)
                {
                    PlayerHasEntered = false;
                    dialogueBox.SetActive(false);
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;
                    other.GetComponent<Player>().inStore = false;
                    return;
                }
                // Display Dialouge
                PlayerHasEntered = true;
                dialogueBox.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                other.GetComponent<Player>().inStore = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerHasEntered = false;
            dialogueBox.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            RemoveEnterText();
        }
    }

    private void ShowEnterText()
    {
        textObject = Instantiate(enterTextPrefab, transform.position + offset, transform.rotation, transform);
        textObject.GetComponent<TextMeshPro>().text = info;
    }

    private void RemoveEnterText()
    {
        Destroy(textObject);
    }

    // Update is called once per frame
    void Update()
    {
       
        
    }



}
