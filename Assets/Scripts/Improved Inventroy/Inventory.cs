﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
public class Inventory : MonoBehaviour
{
    [SerializeField] List<Item> items;
    [SerializeField] Transform itemsParent;
    [SerializeField] ItemSlot[] itemSlots;

    [SerializeField] Item gem;
    [SerializeField] Item fish;
    [SerializeField] Item wood;
    [SerializeField] Item supersword;

    public event Action<Item> OnItemRightCLickEvent;

    private void Start()
    {
        for(int i = 0; i < itemSlots.Length; i++)
        {
            itemSlots[i].OnRightClickEvent += OnItemRightCLickEvent;
        }
    }



    private void OnValidate()
    {
        if (itemsParent != null)
        {
            itemSlots = itemsParent.GetComponentsInChildren<ItemSlot>();
        }

        RefreshUI();
    }

    private void RefreshUI()
    {
        int i = 0;
        for (; i < items.Count && i < itemSlots.Length; i++)
        {
            itemSlots[i].Item = items[i];
        }

        for (; i < itemSlots.Length; i++)
        {
            itemSlots[i].Item = null;
        }
    }

    public bool AddItem(Item item)
    {
        if(IsFull())
        {
            return false;
        }
        items.Add(item);
        RefreshUI();
        return true;
    }
    public bool AddItem(String item)
    {
        Item itemu;
        Debug.Log("receieveveveveevd : " + item);
        if(item == "fish")
        {
            itemu = fish;
        }
        else if (item == "gem")
        {
            itemu = gem;
        }
        else if(item=="supersword")
        {
            itemu = supersword;
        }
        
        else
        {
            itemu = wood;
        }
        if (IsFull())
        {
            return false;
        }
        items.Add(itemu);
        RefreshUI();
        return true;
    }

    public bool RemoveItem(Item item)
    {
        if(items.Remove(item))
        {
            RefreshUI();
            return true;
        }
        return false;
    }
    public bool RemoveItem(string item)
    {
        if (item == "fish")
        {
            Item itemu = fish;
            if (items.Remove(itemu))
            {
                RefreshUI();
                return true;
            }
        }
        if (item == "gem")
        {
            Item itemu = gem;
            if (items.Remove(itemu))
            {
                RefreshUI();
                return true;
            }
        }
        if (item == "wood")
        {
            Item itemu = wood;
            if (items.Remove(itemu))
            {
                RefreshUI();
                return true;
            }
        }
        return false;
    }

    public bool IsFull()
    {
        return items.Count >= itemSlots.Length;
    }

}
