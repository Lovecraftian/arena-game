﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    [SerializeField] Inventory inventroy;
    [SerializeField] EquipmentPanel equipmentPanel;

    private void Awake()
    {
        inventroy.OnItemRightCLickEvent += EquipFromInventory;
        equipmentPanel.OnItemRightCLickEvent += UnequipFromPanel;
    }

    private void EquipFromInventory(Item item)
    {
        if(item is EquippableItem)
        {
            Equip((EquippableItem)item);
        }

    }

    public void Equip(EquippableItem item)
    {
        if(inventroy.RemoveItem(item))
        {
            EquippableItem previousItem;
            if(equipmentPanel.AddItem(item, out previousItem))
            {
                if(previousItem != null)
                {
                    inventroy.AddItem(previousItem);
                }
            }
            else
            {
                inventroy.AddItem(item);
            }
        }
    }

    private void UnequipFromPanel(Item item)
    {
        if(item is EquippableItem)
        {
            Unequip((EquippableItem)item);
        }
    }

    public void Unequip(EquippableItem item)
    {
        // checks that inventory is not full before removing item
        if(!inventroy.IsFull() && equipmentPanel.RemoveItem(item))
        {
            inventroy.AddItem(item);
        }
    }


}
