﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IState
{
    private Animator _animator;
    private GameObject _owner;
    private float _lookRadius;
    private StateMachine _stateMachine;
    private Transform _target; //  = PlayerManager.m_instance.m_player.transform;
    private Transform _ownerTransform;

    public IdleState(Animator i_animator, GameObject i_owner, float i_lookradius, Transform i_target, StateMachine i_statemachine)
    {
        this._animator     = i_animator;
        this._owner        = i_owner;
        this._lookRadius   = i_lookradius;
        this._target       = i_target;
        this._stateMachine = i_statemachine;
    }

    #region States
    public void StateEnter()
    {
        Debug.Log("Enter Idle state");
        _animator.SetBool("isIdle", true);

        /*
        Enemy s = _owner.GetComponent<Enemy>();
        Transform x = s._target;

        Transform lol = _owner.GetComponent<Enemy>()._target;

        int id = _owner.GetComponent<Skelekton>().id; */

    }

    public void StateExit()
    {
        Debug.Log("Exiting Idle state");
        _animator.SetBool("isIdle", false);
        Debug.Log("isIdle should be false now");
    }

    public void StateUpdate()
    {
       // Debug.Log("Updateing Idle state");

        float distance = Vector3.Distance(_target.position, _owner.transform.position);
        // check if player position is with inn view range
        if (distance <= _lookRadius)
        {
            StateExit(); // tbh the state machine will exit when we call the change state 
            _stateMachine.ChangeState(new WalkingState(_owner, _animator)); // Walking State

        }
    }

    #endregion
}
