﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;

public class WalkingState : IState
{
    private Animator _animator;
    private GameObject _owner;
    private NavMeshAgent _agent;
    private Transform _target;

    public WalkingState(GameObject i_owner, Animator i_animator)
    {
        _owner    = i_owner;
        _animator = i_animator;

    }
    #region States
    public void StateEnter()
    {

        _animator.SetBool("isWalking", true);

        // Set target
        _agent = _owner.GetComponent<Skelekton>().m_agent;
        _target = _owner.GetComponent<Skelekton>()._target;
    }

    public void StateExit()
    {
        _animator.SetBool("isWalking", false);
    }

    public void StateUpdate()
    {
        _agent.SetDestination(_target.position);
    }
    #endregion

}
