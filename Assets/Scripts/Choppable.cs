﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Choppable : MonoBehaviour
{
    public float chopTime = 7f;
    private float currentTime = 0f;
    public string logType = "Birch";
    public bool alive = true;
    public bool available = true;
    Player player;
    int damage = 0;
    int amountLogs = 4;

    float currentDeadTime = 0f;
    float deadTime = 10f;

    private void Update()
    {
        if(!alive)
        {
            currentDeadTime += Time.deltaTime;

            if(currentDeadTime >= deadTime)
            {
                currentDeadTime = 0f;
                alive = true;
                damage = 0;
                gameObject.GetComponent<MeshRenderer>().enabled = true;
                gameObject.GetComponent<MeshCollider>().enabled = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!alive) return;
        if (other.CompareTag("Detection"))
        {
            available = false;
            player = other.gameObject.transform.root.gameObject.GetComponent<Player>();
            if (player)
            {
                player.withinChoppingRange = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {

        if (other.CompareTag("Detection"))
        {
            if (!alive) return;
            currentTime += Time.deltaTime;
            if (currentTime >= chopTime)
            {
                //player receives ore
                currentTime = 0f;
                damage++;
                player.GetItem(logType);

                if(damage >= amountLogs)
                {
                    alive = false;
                    damage = 0;
                    gameObject.GetComponent<MeshRenderer>().enabled = false;
                    gameObject.GetComponent<MeshCollider>().enabled = false;
                    player.stopSkill();
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!alive) return;
        if (other.CompareTag("Player"))
        {
            available = true;
            
            if (player)
            {
                player.withinChoppingRange = false;
            }

        }
    }
}
