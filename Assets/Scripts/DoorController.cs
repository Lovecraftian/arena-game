﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    GameObject boss;
    Transform bossSpawn;
    void Start()
    {
        EventSystem.current.onBossTriggerEnter += OnBossSpawn;
    }
    private void OnDestroy()
    {
        EventSystem.current.onBossTriggerEnter -= OnBossSpawn;
    }
    private void OnBossSpawn()
    {
        Instantiate(boss, bossSpawn);
    }
   
}
